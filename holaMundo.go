package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Hola mundo desde Go")
	time.Sleep(time.Second * 2)
	fmt.Println("Adios")
}
